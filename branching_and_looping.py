def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    return list(range(start,end,step))

def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    lst = []
    if start < end:
        while start < end :
            lst.append(start)
            start += step
    else:
        while start > end:
            lst.append(start)
            start += step
    return lst
    
def check_prime(num):
    for i in range(2,num):
        if num % i == 0 :
            return False
    return True


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    prime_number = []
    for i in range(10,100):
        if check_prime(i):
            prime_number.append(i)
    return prime_number
    