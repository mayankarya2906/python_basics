def add3(a, b, c):
    return sum([a, b, c])


def unpacking1():
    items = [1, 2, 3]
    # Call add3 by passing unpacking items
    return add3(*items)


def unpacking2():
    kwargs = {'a': 1, 'b': 2, 'c': 3}
    s = kwargs.values()
    return add3(*s)
    # Call add3 by unpacking kwargs


def call_function_using_keyword_arguments_example():
    a = 1
    b = 2
    c = 3
    # Call add3 by passing a, b and c as keyword arguments
    return add3(a,b,c)


def add_n(*a):
    """
    Define `add_n` so that it accepts
    any number of arguments and
    returns the sum of all the arguments
    """
    return sum(a)


def add_kwargs(**kw):
    """
    Define `add_kwargs` so that it accepts a and b as keyword arguments
    and returns the sume of these arguments.

    Ensure the function raises an exception when arguments apart from a and b
    are passed to `add_kwargs`
    """
    for key , value in kw.items():
        s = 0
        if key != "a" and if key != "b":
            return "invalid input"
        else:
            s = s + value
    return s



def universal_acceptor(*a,**kw):
    """
    Define `universal_acceptor` so that it accepts any kind of
    arguments and keyword-arguments,
    and prints the arguments and keyword-arguments.
    """
    return a, kw
