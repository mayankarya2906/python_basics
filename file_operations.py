def read_file(path):
    f = open(path,'r')
    data = f.read()
    f.close()
    return data
def write_to_file(path, s):
    f = open(path,'w')
    data = f.write(s)
    f.close()
    return data
def append_to_file(path, s):
    f = open(path,'a')
    data =  f.write(s)
    f.close()
    return data
def numbers_and_squares(n, file_path):

    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    l = []
    for i in range(1,n+1):
        l.append(i)
    s = list(map(lambda x: x**2,l))
    f = open(file_path,"w")
    for i in range(0,n):
        data = f.write(str(l[i]) +str(',')+str(s[i])+str('\n'))
    f.close()
    return data