def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    string = s.replace('.','').replace(',','')
    lst_str = string.split(' ')
    set_str = set(lst_str)
    lst_final = list(set_str)
    freq = []
    for i in lst_final:
        freq.append(lst_str.count(i))
    dict_freq = dict(zip(lst_final,freq))
    return dict_freq

def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    return list(d.items())


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    dict_ = sorted(d.items(),key = lambda x:x[0])
    return dict_
