def is_prime(n):
    """
    Check whether a number is prime or not
    """
    if n == 1:
        return False
    else:

        for i in range(2,n):
            if n % i == 0:
                return False
    return True
            


def n_digit_primes(digit = 2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    n = digit
    lst = []
    low = int('1'+'0'*(n-1))
    high = int('9'*(n)) + 1
    for i in range(low,high):
        if is_prime(i):
            lst.append(i)
    return lst


    

