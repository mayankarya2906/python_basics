"""
* Create a class called `Point` which has two instance variables,
`x` and `y` that represent the `x` and `y` co-ordinates respectively. 

* Initialize these instance variables in the `__init__` method

* Define a method, `distance` on `Point` which accepts another `Point` object as an argument and 
returns the distance between the two points.
"""

import math
class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y
    def distance(self,p2):
        return math.sqrt((self.x-p2.x)**2 + (self.y - p2.y)**2)
        



        
