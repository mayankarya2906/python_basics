import random
from random import shuffle
import os
def unique(items):
    """
    Return a set of unique values belonging to the `items` list
    """
    return set(items)


def shuffle(items):
    """
    Shuffle all items in a list
    """
    lst = shuffle(items)
    return lst
def getcwd():
    """
    Get current working directory
    """
    dir = os.getcwd()
    return dir

def mkdir(name):
    """
    Create a directory at the current working directory
    """
    pass 
